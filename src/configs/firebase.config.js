import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyDVqhhp6ccKhPuiafP3PGb-dukGJENuLak",
  authDomain: "yt-project20.firebaseapp.com",
  projectId: "yt-project20",
  storageBucket: "yt-project20.appspot.com",
  messagingSenderId: "446817479725",
  appId: "1:446817479725:web:bea2f1b8ce8da583d9dca4"
};

const app = initializeApp(firebaseConfig);

export const db = getFirestore();
export const auth = getAuth(app)
