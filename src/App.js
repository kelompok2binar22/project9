import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Header from './layouts/Header';
import MainLayout from './layouts/MainLayout';
import Login from './pages/Login';
import Register from './pages/Register';
import Profile from './pages/Profile';
import GamePlay from './pages/Game';
import 'react-toastify/dist/ReactToastify.css';
import ProtectedRoute from './components/ProtectedRoute';
import HomePage from './components/HomePage'

const App = () => {
  return (
    <MainLayout>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path='/' element={<ProtectedRoute> <HomePage /> </ProtectedRoute>} />
          <Route path="/login" element={<Login />} />
          <Route path="/profile" element={<ProtectedRoute> <Profile /> </ProtectedRoute>} />
          <Route path="/register" element={<Register />} />
          <Route path="/game" element={<ProtectedRoute> <GamePlay /> </ProtectedRoute>} />
        </Routes>
      </BrowserRouter>
    </MainLayout>
  );
};

export default App;
